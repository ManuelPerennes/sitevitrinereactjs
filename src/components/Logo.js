import React from 'react';

const Logo = () => {
    return (
        <div>
            <span className="logo">PM</span>
        </div>
    );
};

export default Logo;